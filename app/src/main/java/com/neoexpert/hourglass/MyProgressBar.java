package com.neoexpert.hourglass;

import android.view.*;
import android.graphics.*;

public class MyProgressBar extends View
{

	private Paint p=new Paint();

	private int max;

	private int progress;

	private int color;
	public MyProgressBar(android.content.Context context) {
		super(context);
		init();
	}

    public MyProgressBar(android.content.Context context, android.util.AttributeSet attrs) {
		super(context,attrs);
		init();
	}

    public MyProgressBar(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
		super(context,attrs,defStyleAttr);
		init();
	}

    public MyProgressBar(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr, int defStyleRes) {
		super(context,attrs,defStyleAttr,defStyleRes);
		init();
	}

	public void down()
	{
		setBackgroundColor(Color.BLACK);
		p.setColor(color);
	}

	public void up()
	{
		setBackgroundColor(color);
		p.setColor(Color.BLACK);
	}

	private void init()
	{
		color=Color.parseColor("#ffcc00");
		p.setColor(color);
		p.setStyle(Paint.Style.FILL_AND_STROKE);
		
	}

	public void setProgress(int progress)
	{
		this.progress=progress;
		invalidate();
	}

	public void setMax(int max)
	{
		this.max=max;
	}

	@Override
	protected void onDraw(Canvas canvas)
	{
		canvas.drawRect(0,canvas.getHeight()- canvas.getHeight()*progress/max,canvas.getWidth(),canvas.getHeight(),p);
	}
	
}
