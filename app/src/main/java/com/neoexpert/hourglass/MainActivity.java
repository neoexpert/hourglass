package com.neoexpert.hourglass;

import android.app.*;
import android.os.*;
import android.widget.*;
import android.view.*;
import com.neoexpert.hourglass.MainActivity.*;
import javax.xml.datatype.*;
import android.content.res.*;
import android.hardware.*;
import android.content.*;
import android.media.*;
import android.preference.*;
import android.widget.RadioGroup.*;
import android.view.animation.*;
import android.widget.NumberPicker.*;
import android.graphics.*;
import android.graphics.drawable.*;

public class MainActivity extends Activity
implements
CompoundButton.OnCheckedChangeListener,
OnValueChangeListener,
OnLongClickListener
{

	private boolean longPress;
	public void enableLongPress(View v){
		longPress=true;
		editprefs.putBoolean("longPress",longPress).apply();
	}
	public void enableDeviceOrientation(View v){
		longPress=false;
		editprefs.putBoolean("longPress",longPress).apply();
	}
	private CheckBox cbreset;

	@Override
	public boolean onLongClick(View p1)
	{
		if(!longPress)return false;
		customReverse=!customReverse;
		if (reverse ^ customReverse)
		{
			if (cbreset.isChecked())
				p = 0;
			pb.up();
			//circletime = 0;
		}
		else
		{
			if (cbreset.isChecked())
				p = getDuration();
			pb.down();
			//circletime = 0;
		}
		return true;
	}
	

	private int getDuration(){
		int d=(npM.getValue() * 60 + npS.getValue()) * 1000;
		if(d<=0)d=1;
		return d;
	}
	private boolean hintseen;
	@Override
	public void onValueChange(NumberPicker p1, int previous, int c)
	{
		editprefs.putInt("M",npM.getValue());
		editprefs.putInt("S",npS.getValue());
		editprefs.commit();
		updDr.cancel(true);
		p=getDuration();
		
		
		//p = c * 1000;
		
		//Toast.makeText(getApplicationContext(), Durations[currentDuration] / 60 + " min", Toast.LENGTH_LONG).show();

		//pb.setMax(Durations[currentDuration] * 1000);
		pb.setMax(getDuration());
		//pb.setProgress(c * 1000);


		updDr = new updateDuration();
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
			updDr.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		else
			updDr.execute();
		
	}


	@Override
	public void onCheckedChanged(CompoundButton p1, boolean p2)
	{
		editprefs.putBoolean("sound", p2);
		editprefs.commit();
		// TODO: Implement this method
	}

	
	
	MyProgressBar pb;
	LinearLayout ll;
	OrientationEventListener myOrientationEventListener;
	boolean reverse=false;
	private boolean customReverse=false;
	updateDuration updDr;
	SharedPreferences prefs;
	SharedPreferences.Editor editprefs;
	double angle;
	NumberPicker npS;
	NumberPicker npM;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		//getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
							 WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.main);
		
		prefs = PreferenceManager.getDefaultSharedPreferences(this);
		editprefs = prefs.edit();
		hintseen=prefs.getBoolean("hintseen",false);
		if(hintseen)findViewById(R.id.mainTVHint)
			.setVisibility(View.GONE);
		longPress=prefs.getBoolean("longPress",false);
		if(longPress)
			((RadioButton)findViewById(R.id.longPress)).setChecked(true);
		else
			((RadioButton)findViewById(R.id.deviceOrientation)).setChecked(true);
			
		npS = (NumberPicker)findViewById(R.id.mainNPS);
		npS.setMaxValue(59);
		npS.setValue(prefs.getInt("S",0));
		
		npS.setFormatter(new NumberPicker.Formatter() {
				@Override
				public String format(int value) {
					return String.format("%02d", value);
				}
			});
		//np.setValue(1);
		
		npS.setOnValueChangedListener(this);
		npM = (NumberPicker)findViewById(R.id.mainNPM);
		npM.setMaxValue(59);
		npM.setValue(prefs.getInt("M",1));
		npM.setFormatter(new NumberPicker.Formatter() {
				@Override
				public String format(int value) {
					return String.format("%02d", value);
				}
			});
		npM.setOnValueChangedListener(this);
		
		cbreset=(CheckBox)findViewById(R.id.mainCBReset);
		
		cbs = (CheckBox)findViewById(R.id.mainCBSound);

		cbs.setChecked(prefs.getBoolean("sound", true));
		cbs.setOnCheckedChangeListener(this);
		myOrientationEventListener
			= new OrientationEventListener(this, SensorManager.SENSOR_DELAY_UI){

			@Override
			public void onOrientationChanged(int arg0)
			{
				ll.setRotation((-arg0-90)/180*180);
				if(!longPress)
				// TODO Auto-generated method stub
				if (arg0 > 90 && arg0 < 270)
				{
					if (!(reverse^customReverse))
					{
						if (cbreset.isChecked())
							p = 0;
						circletime = 0;
						pb.up();
					}
					reverse = true;
					
					findViewById(R.id.mainTVHint)
						.setVisibility(View.GONE);
					hintseen=true;
					editprefs.putBoolean("hintseen",hintseen);
					editprefs.commit();
				}
				else
				{
					if (reverse^customReverse)
					{
						if (cbreset.isChecked())
							p = getDuration();
						pb.down();
						circletime = 0;
					}
					reverse = false;
					//ll.setRotation(0);


				}
				//ll.setRotation(-arg0);
				//RotateAnimation r =new RotateAnimation((float)angle, arg0, Animation.RELATIVE_TO_SELF, ll.getWidth()/2, Animation.RELATIVE_TO_SELF, ll.getHeight()/2);
				//r.setDuration(0);
				angle = arg0;
				//ll.startAnimation(r);
				//ll.setAnimation(r);
				//ll.animate();
				//Toast.makeText(getApplicationContext(),"Orientation: " + String.valueOf(arg0),Toast.LENGTH_LONG).show();
			}};

		if (myOrientationEventListener.canDetectOrientation())
		{
			//Toast.makeText(this, "Can DetectOrientation", Toast.LENGTH_LONG).show();
			myOrientationEventListener.enable();
		}
		else
		{
			Toast.makeText(this, "Can't DetectOrientation", Toast.LENGTH_LONG).show();
			//finish();
		}
		//Button b=(Button)findViewById(R.id.timeButton);
		//if (Durations[currentDuration] < 60)
		//b.setText(Durations[currentDuration] + " s");
		//Toast.makeText(getApplicationContext(), Durations[currentDuration] + " s", Toast.LENGTH_LONG).show();
		//else
		//b.setText(Durations[currentDuration] / 60 + " min");


		ll = (LinearLayout)findViewById(R.id.mainLinearLayout1);
		ll.setVisibility(View.GONE);
		
		pb = (MyProgressBar)findViewById(R.id.mainPB);
		
		pb.setOnLongClickListener(this);

		pb.setMax(getDuration());
		pb.setProgress(getDuration());

		updDr = new updateDuration();
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
			updDr.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		else
			updDr.execute();
    }

	int p=0;
	boolean soundPlayed=false;
	CheckBox cbs;//=(CheckBox)findViewById(R.id.mainCBSound);

	private class updateDuration extends AsyncTask<String, Integer, Long>
	{

		

		protected Long doInBackground(String... urls)
		{
			p = getDuration();
			while (!isCancelled())
			{
				int i=100;
				if (i > 1000) i = 1000;
				try
				{
					Thread.sleep(i);
				}
				catch (InterruptedException e)
				{} 
				if ((reverse^customReverse))
				{
					p += i;
					if (p >= getDuration())
					{
						p = getDuration();
						if (!soundPlayed && cbs.isChecked() && visible)
						{
							soundPlayed = true;
							sound();
						}
						publishProgress(1);
						continue;
					}
					else
					{
						soundPlayed = false;
					}
				}
				else
				{
					p -= i;
					if (p <= 0)
					{
						p = 0;
						if (!soundPlayed && cbs.isChecked() && visible)
						{
							soundPlayed = true;
							sound();
						}
						publishProgress(1);
						continue;
					}
					else
					{
						soundPlayed = false;
					}
				}


				publishProgress(1);


			}
			/*p=0;
			 publishProgress(0);
			 */
			return 0L;
		}
		float x=ll.getX();
		float y=ll.getY();
		float a=16;
		protected void onProgressUpdate(Integer... progress)
		{
			pb.setProgress(p);
			TextView tvt=(TextView)findViewById(R.id.mainTVTime);
			tvt.setText(time(p));

		}

		protected void onPostExecute(Long result)
		{


			//showDialog("Downloaded " + result + " bytes");
		}
	}
	animateCircle anc=null;
	public void onClick(View v)
	{
		//sound();
		switch (v.getId())
		{
//			case R.id.timeButton:

			case R.id.mainPB:
				if (anc == null)
					anc = new animateCircle();

			 	ll = (LinearLayout)findViewById(R.id.mainLinearLayout1);
				if (ll.getVisibility() == View.VISIBLE)
				{
					ll.setVisibility(View.GONE);
					anc.cancel(true);
				}
				else
				{
					anc = new animateCircle();
					ll.setVisibility(View.VISIBLE);
					if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
						anc.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
					else
						anc.execute();
				}


				break;

		}
	}

	boolean visible=true;
	@Override
	protected void onResume()
	{
		super.onResume();
		visible = true;
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
	}

	@Override
	protected void onStop()
	{
		super.onStop();
		visible = false;
		getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

	}
	boolean isPlaying=false;
	public void sound()
	{
		if (isPlaying) return;
		isPlaying = true;
		Runnable runnable = new Runnable() {
			@Override
			public void run()
			{
				short[] buffer = new short[44100];
				AudioTrack track = new AudioTrack(AudioManager.STREAM_MUSIC, 44100, AudioFormat.CHANNEL_CONFIGURATION_MONO, AudioFormat.ENCODING_PCM_16BIT, 1024, AudioTrack.MODE_STREAM);


				float samples[] = new float[4410];

				track.play();
				for (int j=0;j < 7;j++)
				{
					for (int i = 0; i < samples.length; i++)
					{
						if (j % 2 == 0)
							samples[i] = (float)  Math.sin((float)i * ((float)(2 * Math.PI) * 1500 / 44100));    //the part that makes this a sine wave....
						else
							samples[i] = 0;// (float)  Math.sin((float)0 * ((float)(2 * Math.PI) * 1500 / 44100));    //the part that makes this a sine wave....

						buffer[i] = (short) (samples[i] * Short.MAX_VALUE);
					}

					track.write(buffer, 0, samples.length);  //write to the audio buffer.... and start all over again!
				}
				isPlaying = false;
			}           


		};
		new Thread(runnable).start();

	}
	public static String time(long t)
	{

		long second = (t / 1000) % 60;
		long minute = (t / (1000 * 60)) % 60;
		long hour = (t / (1000 * 60 * 60)) % 24;


		if (minute == 0 && hour == 0)
			return String.format("%.0f", t / 1000.0f);

		else if (hour == 0)
		 	return String.format("%02d:%02d",  minute, second);
		else
			return String.format("%02d:%02d:%02d", hour, minute, second);

	}
	int circletime=0;
	private class animateCircle extends AsyncTask<String, Integer, Long>
	{

		protected Long doInBackground(String... urls)
		{
			while (!isCancelled())
			{
				try
				{
					Thread.sleep(10);
				}
				catch (InterruptedException e)
				{} 
				circletime += 10;

				publishProgress(1);


			}
			/*p=0;
			 publishProgress(0);
			 */
			return 0L;
		}
		float x=ll.getX();
		float y=ll.getY();
		float a=0;
		float oldY=ll.getY();

		protected void onProgressUpdate(Integer... progress)
		{
			//x+=a*(float)Math.sin(angle/180.0*Math.PI);
			y += a * (float)Math.cos(angle / 180.0 * Math.PI);

			oldY = y;
			a = (float)Math.pow(((double)circletime) / 100.0, (double)2);
			//a*=2;
			//if(x<0)x=0;
			if (y < 0)
			{y = 0;circletime = 0;}
			//if(x>(pb.getWidth()-ll.getWidth()))
			x = (pb.getWidth() - ll.getWidth());
			if (y > (pb.getHeight() - ll.getHeight()))
			{
				y = (pb.getHeight() - ll.getHeight());
				circletime = 0;}
			//ll.setX(x);
			ll.setY(y);
		}

		protected void onPostExecute(Long result)
		{


			//showDialog("Downloaded " + result + " bytes");
		}
	}

}
